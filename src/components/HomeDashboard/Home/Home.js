import React from 'react';
import Dashboard from '../Dashboard/Dashboard';
import Navbar from '../Header/Navbar/Navbar';

const Home = () => {
    return (
        <div>
            <Navbar></Navbar>
            <Dashboard></Dashboard>
        
        </div>
    );
};

export default Home;